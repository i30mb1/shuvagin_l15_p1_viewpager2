ViewPager2 + Infinite ViewPager2 with TabLayout + Custom TabLayout
===============================================

Libraries Used
--------------
* [ViewPager2](https://developer.android.com/jetpack/androidx/releases/viewpager2) - ViewPager2 replaces ViewPager, addressing most of its predecessor’s pain-points, including right-to-left layout support, vertical orientation, modifiable Fragment collections, etc.
* [Foundation](https://developer.android.com/jetpack/components) - Components for core system capabilities, Kotlin extensions and support for multidex and automated testing.
  * [AppCompat](https://developer.android.com/topic/libraries/support-library/packages#v7-appcompat) - Degrade gracefully on older versions of Android.
  * [Android KTX](https://developer.android.com/kotlin/ktx) - Write more concise, idiomatic Kotlin code.
* [Architecture](https://developer.android.com/jetpack/arch/) - A collection of libraries that help you design robust, testable, and maintainable apps. Start with classes for managing your UI component lifecycle and handling data persistence.
    * [Data Binding](https://developer.android.com/topic/libraries/data-binding) - Declaratively bind observable data to UI elements.
    * [Lifecycles](https://developer.android.com/topic/libraries/architecture/lifecycle) - Create a UI that automatically responds to lifecycle events.
* [UI](https://developer.android.com/guide/topics/ui) - Details on why and how to use UI Components in your apps - together or separate
    * [Fragment](https://developer.android.com/guide/components/fragments) - A basic unit of composable UI.
    * [Layout](https://developer.android.com/guide/topics/ui/declaring-layout) - Lay out widgets using different algorithms.
* [Coil](https://github.com/coil-kt/coil) - image loading library with Kotlin idiomatic API

![image](screen1.png)![image](screen2.png)