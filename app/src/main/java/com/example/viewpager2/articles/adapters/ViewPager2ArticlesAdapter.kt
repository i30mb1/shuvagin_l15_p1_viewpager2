package com.example.viewpager2.articles.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpager2.R
import com.example.viewpager2.articles.models.ArticleItemModel
import com.example.viewpager2.databinding.ItemArticleBinding
import com.google.android.material.tabs.TabLayout

class ViewPager2ArticlesAdapter(val tabMainFragment: TabLayout) :
    RecyclerView.Adapter<ViewPager2ArticlesAdapter.ViewPager2Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPager2Holder {
        val itemCardBinding: ItemArticleBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_article,
            parent,
            false
        )

        return ViewPager2Holder(
            itemCardBinding
        )
    }

    override fun getItemCount(): Int = Integer.MAX_VALUE

    override fun onBindViewHolder(holder: ViewPager2Holder, position: Int) {
        when (position % 3) {
            0 -> {
                holder.bindTo(list[0])
            }
            1 -> {
                holder.bindTo(list[1])
            }
            2 -> {
                holder.bindTo(list[0])
            }
        }
    }

    var list: List<ArticleItemModel> = listOf()

    inner class ViewPager2Holder constructor(private val itemArticleBinding: ItemArticleBinding) :
        RecyclerView.ViewHolder(itemArticleBinding.root) {

        fun bindTo(pageItem: ArticleItemModel) {
            itemArticleBinding.item = pageItem
            itemArticleBinding.executePendingBindings()
        }
    }
}
