package com.example.viewpager2.articles.models

import android.graphics.drawable.Drawable

data class ArticleItemModel(var image: Drawable?, var title: String, var sumamary: String, var toolbarTitle: String)
