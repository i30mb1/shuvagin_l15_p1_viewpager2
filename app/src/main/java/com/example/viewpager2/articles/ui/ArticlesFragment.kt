package com.example.viewpager2.articles.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpager2.R
import com.example.viewpager2.articles.adapters.ViewPager2ArticlesAdapter
import com.example.viewpager2.articles.models.ArticleItemModel
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.main_fragment.*

class ArticlesFragment : Fragment() {

    companion object {
        fun newInstance() = ArticlesFragment()
    }

    private lateinit var viewModel: ArticlesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ArticlesViewModel::class.java)
        setupViewPager2()
    }

    private fun setupViewPager2() {
        val adapter = ViewPager2ArticlesAdapter(tab_main_fragment)
        vp2_main_fragment.adapter = adapter
        vp2_main_fragment.offscreenPageLimit = 4
        vp2_main_fragment.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        vp2_main_fragment.setPageTransformer { page, position ->
            page.apply {
                //                scaleX = 1f + position
                //                scaleY = 1f + position
            }
        }
        populateAdapter(adapter)

        TabLayoutMediator(
            3,
            tab_main_fragment,
            vp2_main_fragment,
            tabConfigurationStrategy = object : TabLayoutMediator.TabConfigurationStrategy {
                override fun onConfigureTab(tab: TabLayout.Tab, position: Int) {
                }
            }).attach()

        vp2_main_fragment.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position % 3) {
                    0 -> updateTabSelection(0)
                    1 -> updateTabSelection(1)
                    2 -> updateTabSelection(2)
                }
            }
        })
    }

    fun updateTabSelection(position: Int) {
        // get the position of the currently selected tab and set selected to false
        tab_main_fragment.getTabAt(tab_main_fragment.selectedTabPosition)?.customView?.isSelected = false
        // set selected to true on the desired tab
        tab_main_fragment.getTabAt(position)?.customView?.isSelected = true
        // move the selection indicator
        tab_main_fragment.setScrollPosition(position, 0f, true)
        // ... your logic to swap out your fragments
    }

    private fun populateAdapter(adapter: ViewPager2ArticlesAdapter) {
        adapter.list = listOf(
            ArticleItemModel(
                ContextCompat.getDrawable(
                    requireContext(), R.drawable.here_we_go_again
                ),
                getString(R.string.article_item1_title),
                getString(R.string.article_item1_summary),
                getString(R.string.article_item1_toolbar_title)
            ),
            ArticleItemModel(
                ContextCompat.getDrawable(
                    requireContext(), R.drawable.weather_child
                ),
                getString(R.string.article_item2_title),
                getString(R.string.article_item2_summary),
                getString(R.string.article_item2_toolbar_title)
            ),
            ArticleItemModel(
                ContextCompat.getDrawable(
                    requireContext(), R.drawable.here_we_go_again
                ),
                getString(R.string.article_item1_title),
                getString(R.string.article_item1_summary),
                getString(R.string.article_item1_toolbar_title)
            )
        )
    }
}
