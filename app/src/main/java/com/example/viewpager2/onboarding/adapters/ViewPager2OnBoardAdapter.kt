package com.example.viewpager2.onboarding.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpager2.R
import com.example.viewpager2.articles.ui.ArticlesFragment
import com.example.viewpager2.databinding.ItemOnboardBinding
import com.example.viewpager2.onboarding.models.OnBoardItem
import com.example.viewpager2.onboarding.ui.MainFragment

class ViewPager2OnBoardAdapter(var mainFragment: MainFragment) :
    RecyclerView.Adapter<ViewPager2OnBoardAdapter.ViewPager2Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPager2Holder {
        val itemCardBinding: ItemOnboardBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_onboard,
            parent,
            false
        )

        return ViewPager2Holder(
            itemCardBinding
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewPager2Holder, position: Int) =
        holder.bindTo(list[position])

    var list: List<OnBoardItem> = listOf()

    inner class ViewPager2Holder constructor(private val itemCardBinding: ItemOnboardBinding) :
        RecyclerView.ViewHolder(itemCardBinding.root) {

        fun bindTo(pageItem: OnBoardItem) {
            itemCardBinding.item = pageItem
            itemCardBinding.executePendingBindings()
            itemCardBinding.button.setOnClickListener {
                mainFragment.activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.container, ArticlesFragment.newInstance())
                    ?.addToBackStack(null)
                    ?.commitAllowingStateLoss()
            }
        }
    }
}
