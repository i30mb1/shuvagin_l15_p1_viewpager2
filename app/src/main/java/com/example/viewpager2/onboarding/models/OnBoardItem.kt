package com.example.viewpager2.onboarding.models

data class OnBoardItem(var title: String, var message: String, var buttonVisibility: Boolean = false)
