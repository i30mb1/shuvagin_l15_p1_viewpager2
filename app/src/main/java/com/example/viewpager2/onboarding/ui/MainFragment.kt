package com.example.viewpager2.onboarding.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpager2.R
import com.example.viewpager2.onboarding.adapters.ViewPager2OnBoardAdapter
import com.example.viewpager2.onboarding.models.OnBoardItem
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        setupViewPager2()
    }

    private fun setupViewPager2() {
        val adapter = ViewPager2OnBoardAdapter(this)
        vp2_main_fragment.adapter = adapter
        vp2_main_fragment.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        vp2_main_fragment.setPageTransformer { page, position ->
            page.apply {
                //                scaleX = 1f + position
                //                scaleY = 1f + position
            }
        }
        populateAdapter(adapter)

        TabLayoutMediator(tab_main_fragment, vp2_main_fragment) { tab, position ->
        }.attach()
    }

    private fun populateAdapter(adapter: ViewPager2OnBoardAdapter) {
        adapter.list = listOf(
            OnBoardItem(
                getString(R.string.page_item1_title),
                getString(R.string.page_item1_message)
            ),
            OnBoardItem(
                getString(R.string.page_item2_title),
                getString(R.string.page_item2_message)
            ),
            OnBoardItem(
                getString(R.string.page_item3_title),
                getString(R.string.page_item3_message),
                true
            )
        )
    }
}
