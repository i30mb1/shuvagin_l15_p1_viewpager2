package com.example.viewpager2.utils

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.api.load

@BindingAdapter("setImage")
fun ImageView.setDrawable(image: Drawable) {
    image.let {
        this.load(image)
    }
}
